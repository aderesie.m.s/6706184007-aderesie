package com.example.user.appsmanajemenkeuangan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth auth;
    private Button pemasukan, pengeluaran, hPemasukan, hPengeluaran, logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Bundle bundle = getIntent().getBundleExtra("emailpass");
        String email = bundle.getString("email");

        auth = FirebaseAuth.getInstance();
        TextView User = findViewById(R.id.user);
        User.setText(email);

        pemasukan = findViewById(R.id.btnPemasukan);
        pengeluaran = findViewById(R.id.btnPengeluaran);
        hPemasukan = findViewById(R.id.btnHPemasukan);
        hPengeluaran = findViewById(R.id.btnHPengeluaran);
        logout = findViewById(R.id.btnLogout);

        pemasukan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, pemasukan.class);
                startActivity(intent);
            }
        });



    }

    @Override
    protected void onPause() {
        super.onPause();
        auth.signOut();
    }

}
