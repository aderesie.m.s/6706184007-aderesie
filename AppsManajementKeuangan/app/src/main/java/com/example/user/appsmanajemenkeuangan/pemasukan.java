package com.example.user.appsmanajemenkeuangan;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class pemasukan extends AppCompatActivity {

    private EditText namaPemasukan, tanggalPemasukan, jumlahPemasukan;
    private Button submit;
    private DatabaseReference database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pemasukan);

        initView();
        CreateData();

    }

    private void initView(){

        namaPemasukan = findViewById(R.id.namaAnggaran);
        tanggalPemasukan = findViewById(R.id.tanggalAnggaran);
        jumlahPemasukan = findViewById(R.id.jumlahAnggaran);
        submit = findViewById(R.id.btnSubmit);

    }

    private void CreateData(){

        final String pemasukan = namaPemasukan.getText().toString().trim();
        final String datePemasukan = tanggalPemasukan.getText().toString().trim();
        final String merit = jumlahPemasukan.getText().toString().trim();

        database = FirebaseDatabase.getInstance().getReference();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!pemasukan.isEmpty() && !datePemasukan.isEmpty() && merit.isEmpty()){
                    submitAnggaran(new dataKeuangan(pemasukan,datePemasukan,merit));
                } else {
                    Snackbar.make(findViewById(R.id.btnSubmit),"Data Anggaran tidak boleh kosong",Snackbar.LENGTH_LONG).show();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(namaPemasukan.getWindowToken(),0);
                }
            }
        });

    }

    private void submitAnggaran(dataKeuangan keuangan){
        database.child("keuangan").push().setValue(keuangan).addOnCompleteListener(this, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                namaPemasukan.setText("");
                tanggalPemasukan.setText("");
                jumlahPemasukan.setText("");
                Snackbar.make(findViewById(R.id.btnSubmit),"Data berhasil ditemukan",Snackbar.LENGTH_LONG).show();
            }
        });
    }

    public static Intent getActIntent(Activity activity){
        return new Intent(activity, pemasukan.class);
    }

}
