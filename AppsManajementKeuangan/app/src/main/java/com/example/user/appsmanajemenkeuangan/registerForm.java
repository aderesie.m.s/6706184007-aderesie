package com.example.user.appsmanajemenkeuangan;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.regex.Pattern;

public class registerForm extends AppCompatActivity {

    // object yang dibutuhkan
    private EditText emailRegister, passwordRegister;
    private Button btnRegister;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_form);

        initView();
        register_user();

    }

    // deklarasi object
    private void initView(){
        emailRegister = findViewById(R.id.registerEmail);
        passwordRegister = findViewById(R.id.registerPassword);
        btnRegister = findViewById(R.id.buttonRegister);
        auth = FirebaseAuth.getInstance();
    }


    private void register_user(){
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userEmail = emailRegister.getText().toString().trim();
                String userPassword = passwordRegister.getText().toString().trim();

                if (userEmail.isEmpty()){
                    emailRegister.setError("Email tidak boleh kosong");
                } else if (!Patterns.EMAIL_ADDRESS.matcher(userEmail).matches()){
                    emailRegister.setError("Email tidak valid");
                } else if (userPassword.isEmpty()){
                    passwordRegister.setError("Password tidak boleh kosong");
                } else if (userPassword.length() < 6){
                    passwordRegister.setError("Password kurang dari 6 karakter");
                } else {
                    auth.createUserWithEmailAndPassword(userEmail,userPassword).addOnCompleteListener(registerForm.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (!task.isSuccessful()){
                                Toast.makeText(registerForm.this,"Register Gagal Karena "+task.getException().getMessage(),Toast.LENGTH_LONG).show();
                            } else {
                                startActivity(new Intent(registerForm.this,loginRegister.class));
                            }
                        }
                    });
                }
            }
        });
    }
}
