package com.example.user.appsmanajemenkeuangan;

import java.io.Serializable;

public class dataKeuangan implements Serializable {

    private String namaAnggaran;
    private String tanggal;
    private String jumlah;
    private String key;

    public dataKeuangan(String namaAnggaran, String tanggal, String jumlah) {
        this.namaAnggaran = namaAnggaran;
        this.tanggal = tanggal;
        this.jumlah = jumlah;
    }

    public String getKey(){
        return key;
    }

    public void setKey(){
        this.key = key;
    }

    public String getNamaAnggaran() {
        return namaAnggaran;
    }

    public void setNamaAnggaran(String namaAnggaran) {
        this.namaAnggaran = namaAnggaran;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    @Override
    public String toString() {
        return "dataKeuangan{" +
                "namaAnggaran='" + namaAnggaran + '\'' +
                ", tanggal='" + tanggal + '\'' +
                ", jumlah='" + jumlah + '\'' +
                '}';
    }
}
