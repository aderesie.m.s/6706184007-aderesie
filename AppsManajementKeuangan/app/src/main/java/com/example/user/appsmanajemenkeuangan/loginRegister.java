package com.example.user.appsmanajemenkeuangan;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaCodec;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class loginRegister extends AppCompatActivity {

    private EditText userEmail, userPassword;
    private Button btnLogin, btnRegister;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_register);

        initView();
        login();
    }

    private void initView(){
        userEmail = findViewById(R.id.loginEmail);
        userPassword = findViewById(R.id.loginPassword);
        btnLogin = findViewById(R.id.buttonLogin);
        btnRegister = findViewById(R.id.buttonDaftar);
        auth = FirebaseAuth.getInstance();
    }

    private void login(){

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(loginRegister.this, registerForm.class));
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String emailUser = userEmail.getText().toString().trim();
                final String passwordUser = userPassword.getText().toString().trim();

                if (emailUser.isEmpty()){
                    userEmail.setError("Email Kosong");
                } else if (!Patterns.EMAIL_ADDRESS.matcher(emailUser).matches()){
                    userEmail.setError("Email tidak valid");
                } else if (passwordUser.isEmpty()){
                    userPassword.setError("Password Kosong");
                } else if (passwordUser.length() < 6){
                    userPassword.setError("Password lebih dari 6 karakter");
                } else {
                    auth.signInWithEmailAndPassword(emailUser, passwordUser).addOnCompleteListener(loginRegister.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (!task.isSuccessful()){
                                Toast.makeText(loginRegister.this,"Login karena "+task.getException().getMessage(),Toast.LENGTH_LONG).show();
                            } else {
                                Bundle bundle = new Bundle();
                                bundle.putString("email",emailUser);
                                bundle.putString("pass",passwordUser);
                                startActivity(new Intent(loginRegister.this,MainActivity.class).putExtra("emailpass",bundle));
                                finish();
                            }
                        }
                    });
                }

            }
        });

    }
}
